import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { NavParams } from "ionic-angular";

import { Storage } from "@ionic/storage";
import { GetStartedPage } from "../get-started/get-started";
import { LessonListPage } from "../lesson-list/lesson-list";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  constructor(
    public navCtrl: NavController,
    private storage: Storage,
    public navParams: NavParams
  ) {}

  onClickDepa() {
    var level = {
      level_id: 1,
      level_name: "Departures"
    };
    this.navCtrl.push(LessonListPage, level);
  }

  onClickCon() {
    var level = {
      level_id: 2,
      level_name: "Connections"
    };
    this.navCtrl.push(LessonListPage, level);
  }

  onClickDes() {
    var level = {
      level_id: 3,
      level_name: "Destinations"
    };
    this.navCtrl.push(LessonListPage, level);
  }

  onClickLogout() {
    this.storage.remove("user_id");
    this.storage.remove("token");
    this.navCtrl.setRoot(GetStartedPage);
  }
}
