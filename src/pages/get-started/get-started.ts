import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { LoginPage } from "../login/login";
import { SlideGetStartedPage } from "../slide-get-started/slide-get-started";

@IonicPage()
@Component({
  selector: "page-get-started",
  templateUrl: "get-started.html"
})
export class GetStartedPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  onClickGetStarted() {
    this.navCtrl.push(SlideGetStartedPage);
  }

  onClickLogin() {
    this.navCtrl.push(LoginPage);
  }
}
