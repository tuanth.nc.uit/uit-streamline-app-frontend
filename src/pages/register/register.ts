import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { HttpClient } from "@angular/common/http";
import { AlertController } from "ionic-angular";
import * as URL from "../../baseUrl/URL";
import { timeout, catchError } from "rxjs/operators";
import { LoginPage } from "../login/login";
import { GetStartedPage } from "../get-started/get-started";
import * as $ from "jquery";

@IonicPage()
@Component({
  selector: "page-register",
  templateUrl: "register.html"
})
export class RegisterPage {
  username_register: string = "";
  password_register: string = "";
  retype_password_register: string = "";
  label: string = "";
  message_error: string = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private httpClient: HttpClient,
    public alertCtrl: AlertController,
    public loadingController: LoadingController
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad RegisterPage");
  }

  showNotification() {
    const alert = this.alertCtrl.create({
      title: "Notification!",
      subTitle: `${this.message_error}`,
      buttons: ["OK"]
    });
    alert.present();
  }

  async sendRegisterRequest() {
    let postData = {
      username: this.username_register,
      password: this.password_register
    };

    const loading = this.loadingController.create({
      content: "Waiting...",
      spinner: "bubbles"
    });
    await loading.present();

    this.httpClient
      .post(`${URL.HOST}${URL.SIGNUP}`, postData)
      .pipe(
        timeout(5000),
        catchError(error => {
          if (error.status == 400) {
            console.log(error);
            this.label = "* The Username already exists! ";
          } else {
            this.label = "";
            this.message_error = "The connection has timed out.";
            this.showNotification();
          }
          (<HTMLButtonElement>(
            document.getElementsByClassName("btnRegister")[0]
          )).disabled = false;
          loading.dismiss();
          return error;
        })
      )
      .subscribe(
        data => {
          loading.dismiss();
          this.navCtrl.setRoot(GetStartedPage);
          this.navCtrl.push(LoginPage);
          console.log(data["error"]);
        },
        error => {}
      );
  }

  onClickBtnRegister() {
    if (this.username_register == "" || this.password_register == "" || this.retype_password_register=="") {
      this.label = "* The username or password can't be empty!";
    } else {
      if (this.password_register != this.retype_password_register) {
        this.label = "* Password and Confirm password must match!";
      } else {
        (<HTMLButtonElement>(
          document.getElementsByClassName("btnRegister")[0]
        )).disabled = true;
        this.sendRegisterRequest();
      }
    }
  }

  ngAfterViewInit() {
    $(".txtb input").on("focus", function() {
      $(this).addClass("focus");
    });

    $(".txtb input").on("blur", function() {
      if ($(this).val() == "") $(this).removeClass("focus");
    });
  }
}
