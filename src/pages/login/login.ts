import { Component } from "@angular/core";
import { NavController, NavParams, LoadingController } from "ionic-angular";
import { HomePage } from "../home/home";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import * as URL from "../../baseUrl/URL";
import { RegisterPage } from "../register/register";
import { AlertController } from "ionic-angular";
import { timeout, catchError } from "rxjs/operators";
import * as $ from "jquery";

@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  username_login: string = "";
  password_login: string = "";
  label: string = "";
  user_id: number;
  message_error: string = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: HttpClient,
    private storage: Storage,
    public alertCtrl: AlertController,
    public loadingController: LoadingController
  ) {}

  showNotification() {
    const alert = this.alertCtrl.create({
      title: "Notification!",
      subTitle: `${this.message_error}`,
      buttons: ["OK"]
    });
    alert.present();
  }

  async sendLoginRequest() {
    let postData = {
      username: this.username_login,
      password: this.password_login
    };

    const loading = this.loadingController.create({
      content: "Waiting...",
      spinner: "bubbles"
    });
    await loading.present();

    let headers = new HttpHeaders({
      "token": "a"
    });

    this.http
      .post(`${URL.HOST}${URL.LOGIN}`, postData, {
        headers: headers
      })
      .pipe(
        timeout(5000),
        catchError(error => {
          console.log(error);
          if (error.status == 401) {
            this.label = "* Username or password is incorrect!";
          } else {
            this.label = "";
            this.message_error = "The connection has timed out.";
            this.showNotification();
          }

          (<HTMLButtonElement>(
            document.getElementsByClassName("btnLogin")[0]
          )).disabled = false;

          loading.dismiss();

          return error;
        })
      )
      .subscribe(
        data => {
          console.log(data["data"].UserInfo.ID);
          console.log(data["data"].Token);

          loading.dismiss();
          this.navCtrl.setRoot(HomePage);

          this.storage.set("user_id", data["data"].UserInfo.ID);
          this.storage.set("token", data["data"].Token);
        },
        error => {
          console.log("This is error");
        }
      );
  }

  onClickBtnLogin() {
    if(this.username_login==""||this.password_login==""){
      this.label="* The username or password can't be empty!";
    }
    else{
      this.label="";
      (<HTMLButtonElement>(
        document.getElementsByClassName("btnLogin")[0]
      )).disabled = true;
      this.sendLoginRequest();
    }
  }

  onClickSignUp() {
    this.navCtrl.push(RegisterPage);
  }

  ngAfterViewInit() {
    $(".txtb input").on("focus", function() {
      $(this).addClass("focus");
    });

    $(".txtb input").on("blur", function() {
      if ($(this).val() == "") $(this).removeClass("focus");
    });
  }
}
