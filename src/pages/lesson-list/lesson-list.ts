import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { LessonPage } from "../lesson/lesson";
import { KnowledgeTestPage } from "../knowledge-test/knowledge-test";
import * as constant from "../../baseUrl/URL";
import { AlertController } from "ionic-angular";
import { timeout, catchError } from "rxjs/operators";

@IonicPage()
@Component({
  selector: "page-lesson-list",
  templateUrl: "lesson-list.html"
})
export class LessonListPage {
  level_id: number;
  level_name: string;
  user_id: number;
  token: string;
  list_data: any[];
  lessonID_start: any;
  lessonID_end: any;
  message_error: string = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private http: HttpClient,
    public alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad LessonListPage");
  }

  ionViewWillEnter() {
    this.sendLessonListRequest();
  }

  showNotification() {
    const alert = this.alertCtrl.create({
      title: "Notification!",
      subTitle: `${this.message_error}`,
      buttons: ["OK"]
    });
    alert.present();
  }

  async sendLessonListRequest() {
    this.level_id = this.navParams.data.level_id;
    this.level_name = this.navParams.data.level_name;

    await this.storage.get("user_id").then(val => {
      this.user_id = val;
    });

    await this.storage.get("token").then(val => {
      this.token = val;
    });

    let headers = new HttpHeaders({
      "token": this.token,
    });

    this.http
      .get(
        `${constant.HOST}${constant.LESSON_LIST}/${this.user_id}/${this.level_id}`,
        {
          headers: headers
        }
      )
      .pipe(
        timeout(5000),
        catchError(error => {
          if(error.status == 401){
            this.message_error = "Unauthorized.";
          this.showNotification();
          return error;
          }
          this.message_error = "The connection has timed out.";
          this.showNotification();
          console.log(error);
          return error;
        })
      )
      .subscribe(
        data => {
          this.list_data = data["data"];
          this.list_data.sort(function(a, b) {
            return a.ID - b.ID;
          });
          this.lessonID_start=data["data"][0].ID;
          this.lessonID_end=data["data"][data["data"].length-1].ID;
        },
        error => {}
      );
  }

  ngOnInit() {}

  onClickLearn(lesson_id) {
    var data = {
      lessonID_current: lesson_id,
      lessonID_start: this.lessonID_start,
      lessonID_end: this.lessonID_end
    };
    this.navCtrl.push(LessonPage, data);
  }

  onClickTest(lesson_id, i) {
    var list = {
      lesson_id: lesson_id,
      lesson_name: this.list_data[i].Name
    };
    this.navCtrl.push(KnowledgeTestPage, list);
  }
}
