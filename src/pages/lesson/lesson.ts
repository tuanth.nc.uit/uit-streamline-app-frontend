import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import * as URL from "../../baseUrl/URL";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AlertController } from "ionic-angular";
import { timeout, catchError } from "rxjs/operators";
import { Storage } from "@ionic/storage";

@IonicPage()
@Component({
  selector: "page-lesson",
  templateUrl: "lesson.html"
})
export class LessonPage {
  imageContent: any = "";
  iconPause: any =
    '<ion-icon name="pause" role="img" class="icon icon-md ion-md-pause" aria-label="pause" ng-reflect-name="pause"></ion-icon>';
  audioContent: any;
  iconPlay: any =
    '<ion-icon name="play" role="img" class="icon icon-md ion-md-play" aria-label="play" ng-reflect-name="play"></ion-icon>';

  spa: any;
  aud: any;
  lessonID_current: any;
  lessonID_start: any;
  lessonID_end: any;
  contentLesson: any;
  lessonTitle: any = "";
  button_color: any[] = ["#dcdde1", "#fff", "#fff"];
  button_repeat: any;
  message_error: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: HttpClient,
    private storage: Storage,
    public alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad LessonPage");
  }

  showNotification() {
    const alert = this.alertCtrl.create({
      title: "Notification!",
      subTitle: `${this.message_error}`,
      buttons: ["OK"]
    });
    alert.present();
  }

  onclickConv() {
    this.button_color[0] = "#dcdde1";
    this.button_color[1] = "#fff";
    this.button_color[2] = "#fff";
    this.imageContent =
      `${URL.HOST}/` + this.contentLesson.ImageContent[2] + ".jpeg";
  }

  onclickVocab() {
    this.button_color[0] = "#fff";
    this.button_color[1] = "#dcdde1";
    this.button_color[2] = "#fff";
    this.imageContent =
      `${URL.HOST}/` + this.contentLesson.ImageContent[1] + ".jpeg";
  }

  onclickGramm() {
    this.button_color[0] = "#fff";
    this.button_color[1] = "#fff";
    this.button_color[2] = "#dcdde1";
    this.imageContent =
      `${URL.HOST}/` + this.contentLesson.ImageContent[0] + ".jpeg";
  }

  async sendLessonContentRequest() {
    let headers: any;
    await this.storage.get("token").then(val => {
      headers = new HttpHeaders({
        token: val
      });
    });

    this.http
      .get(`${URL.HOST}${URL.LESSON_CONTENT}/${this.lessonID_current}`, {
        headers: headers
      })
      .pipe(
        timeout(5000),
        catchError(error => {
          this.message_error = "Oops! We have some errors :(";
          this.showNotification();
          return error;
        })
      )
      .subscribe(
        data => {
          this.contentLesson = data["data"];
          this.lessonTitle = this.contentLesson.Name;
          this.audioContent =
            `${URL.HOST}/` + this.contentLesson.Audio + ".mp3";
          this.imageContent =
            `${URL.HOST}/` + this.contentLesson.ImageContent[2] + ".jpeg";
        },
        error => {}
      );
  }

  ngOnInit() {
    this.lessonID_start = this.navParams.data.lessonID_start;
    this.lessonID_end = this.navParams.data.lessonID_end;
    this.lessonID_current = this.navParams.data.lessonID_current;
    this.sendLessonContentRequest();

    this.spa = <HTMLButtonElement>(
      document.getElementById("play-pause").getElementsByTagName("span")[0]
    );
    this.aud = <HTMLAudioElement>document.getElementById("myAudio");
    this.button_repeat = <HTMLButtonElement>document.getElementById("repeat");

    // listener audio, change icon when click play audio
    // The addEventListener() method attaches an event handler to the specified element
    this.aud.addEventListener(
      "play",
      function() {
        this.spa.innerHTML = this.iconPause;
      }.bind(this)
    );

    // listener audio, change icon when click pause audio
    this.aud.addEventListener(
      "pause",
      function() {
        this.spa.innerHTML = this.iconPlay;
      }.bind(this)
    );

    // The ended event occurs when the audio has reached the end.
    this.aud.onended = () => {
      this.spa.innerHTML = this.iconPlay;
    };
  }

  onclickAudioControl() {
    if (this.spa.innerHTML == this.iconPause) {
      this.spa.innerHTML = this.iconPlay;
      this.aud.pause();
    } else {
      this.spa.innerHTML = this.iconPause;
      this.aud.play();
    }
  }

  onclickAudioRepeat() {
    if (this.aud.loop == true) {
      this.aud.loop = false;
      this.button_repeat.style.backgroundColor = "#fff";
    } else {
      this.aud.loop = true;
      this.button_repeat.style.backgroundColor = "#dcdde1";
    }
  }

  onClickBackward() {
    this.spa.innerHTML = this.iconPlay;
    this.button_color[0] = "#dcdde1";
    this.button_color[1] = "#fff";
    this.button_color[2] = "#fff";

    if (this.lessonID_current > this.lessonID_start) {
      this.lessonID_current = this.lessonID_current - 1;
      this.sendLessonContentRequest();
    }
  }

  onClickForward() {
    this.spa.innerHTML = this.iconPlay;
    this.button_color[0] = "#dcdde1";
    this.button_color[1] = "#fff";
    this.button_color[2] = "#fff";

    if (this.lessonID_current < this.lessonID_end) {
      this.lessonID_current = this.lessonID_current + 1;
      this.sendLessonContentRequest();
    }
  }
}
