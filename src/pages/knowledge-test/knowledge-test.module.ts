import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KnowledgeTestPage } from './knowledge-test';

@NgModule({
  declarations: [
    KnowledgeTestPage,
  ],
  imports: [
    IonicPageModule.forChild(KnowledgeTestPage),
  ],
})
export class KnowledgeTestPageModule {}
