import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import * as URL from "../../baseUrl/URL";
import { AlertController } from "ionic-angular";
import { LoadingController } from "ionic-angular";
import { timeout, catchError } from "rxjs/operators";

@IonicPage()
@Component({
  selector: "page-knowledge-test",
  templateUrl: "knowledge-test.html"
})
export class KnowledgeTestPage {
  lesson_id: number;
  user_id: any;
  questions_list: any = [];
  number_question: any;
  lesson_name: any = "";
  answers_list_user: any = [];
  answers_list_server: any = [];
  answerColor: any[][] = [];
  question: any[] = [];
  point: any = 0;
  token: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: HttpClient,
    private storage: Storage,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {}

  showError() {
    const alert = this.alertCtrl.create({
      title: "Notification!",
      subTitle: "The connection has timed out.",
      buttons: ["OK"]
    });
    alert.present();
  }

  showTestResult() {
    console.log(this.point);
    const confirm = this.alertCtrl.create({
      title: `You got ${this.point}/${this.number_question} points`,
      message: "Would you like to return to the lesson list?",
      buttons: [
        {
          text: "Check",
          handler: () => {
            this.checkUserAnswers();
          }
        },
        {
          text: "Test Again",
          handler: () => {
            console.log("Agree clicked");
            for (var i = 0; i < this.number_question; i++) {
              this.answers_list_user[i].questionid = null;
              this.answers_list_user[i].answer = null;
              for (var j = 0; j < 4; j++) {
                this.answerColor[i][j] = "#fff";
              }
            }

            this.sendQuestionListRequest();
          }
        },
        {
          text: "OK",
          handler: () => {
            console.log("Agree clicked");
            this.navCtrl.pop();
          }
        }
      ],
      cssClass: "alert"
    });
    confirm.present();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad KnowledgeTestPage");
  }

  // call API to get a list of questions
  async sendQuestionListRequest() {
    await this.storage.get("user_id").then(val => {
      this.user_id = val;
    });

    await this.storage.get("token").then(val => {
      this.token = val;
    });

    let headers = new HttpHeaders({
      token: this.token
    });

    this.http
      .get(`${URL.HOST}${URL.QUESTION}/${this.lesson_id}`, { headers: headers })
      .pipe(
        timeout(5000),
        catchError(error => {
          this.showError();
          return error;
        })
      )
      .subscribe(
        data => {
          this.questions_list = data["data"];
          console.log(this.questions_list);
          this.number_question = this.questions_list.length;
          this.questions_list.sort(function(a, b) {
            return a.ID - b.ID;
          });

          var temp = this.number_question;
          while (temp > 0) {
            var answer = {
              questionid: null,
              answer: null
            };
            var color = ["#fff", "#fff", "#fff", "#fff"];
            var question_id = `question${temp}`;
            this.answers_list_user.push(answer);
            this.answerColor.push(color);
            this.question.push(question_id);
            temp--;
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  // call API to check the user answer
  async sendCheckUserAnswerRequest() {
    await this.storage.get("user_id").then(val => {
      this.user_id = val;
    });

    await this.storage.get("token").then(val => {
      this.token = val;
    });

    let headers = new HttpHeaders({
      token: this.token
    });

    // this.answers_list_user.sort(function(a,b){return a.questionid - b.questionid});
    console.log(this.answers_list_user);
    var answers_list = {
      clientanswer: this.answers_list_user
    };

    this.http
      .post(
        `${URL.HOST}${URL.CHECK_ANSWER}/${this.user_id}/${this.lesson_id}`,
        answers_list,
        {
          headers: headers
        }
      )
      .pipe(
        timeout(5000),
        catchError(error => {
          this.showError();
          return error;
        })
      )
      .subscribe(
        data => {
          this.answers_list_server = data["data"].checkanswer;
          this.point = data["data"].correctedtotal;
          (<HTMLButtonElement>(
            document.getElementsByClassName("btn-submit")[0]
          )).disabled = false;

          this.showTestResult();
        },
        error => {}
      );
  }

  ngOnInit() {
    this.lesson_id = this.navParams.data.lesson_id;
    this.lesson_name = this.navParams.data.lesson_name;
    console.log(this.lesson_name);
    this.sendQuestionListRequest();
  }

  // handle when user click on an answer
  onClickSelected(index, questionid, answerid) {
    if (answerid == 0) {
      this.answerColor[index][0] = "#a4b0be";
      this.answerColor[index][1] = "#fff";
      this.answerColor[index][2] = "#fff";
      this.answerColor[index][3] = "#fff";

      this.answers_list_user[index].questionid = questionid;
      this.answers_list_user[index].answer = "A";
    } else if (answerid == 1) {
      this.answerColor[index][0] = "#fff";
      this.answerColor[index][1] = "#a4b0be";
      this.answerColor[index][2] = "#fff";
      this.answerColor[index][3] = "#fff";

      this.answers_list_user[index].questionid = questionid;
      this.answers_list_user[index].answer = "B";
    } else if (answerid == 2) {
      this.answerColor[index][0] = "#fff";
      this.answerColor[index][1] = "#fff";
      this.answerColor[index][2] = "#a4b0be";
      this.answerColor[index][3] = "#fff";

      this.answers_list_user[index].questionid = questionid;
      this.answers_list_user[index].answer = "C";
    } else {
      this.answerColor[index][0] = "#fff";
      this.answerColor[index][1] = "#fff";
      this.answerColor[index][2] = "#fff";
      this.answerColor[index][3] = "#a4b0be";

      this.answers_list_user[index].questionid = questionid;
      this.answers_list_user[index].answer = "D";
    }
  }

  // change the answer color for the user's answer
  checkUserAnswers() {
    for (var i = 0; i < this.number_question; i++) {
      if (this.answers_list_server[i].correct == true) {
        if (this.answers_list_user[i].answer == "A") {
          this.answerColor[i][0] = "#32ff7e";
          this.answerColor[i][1] = "#fff";
          this.answerColor[i][2] = "#fff";
          this.answerColor[i][3] = "#fff";
        } else if (this.answers_list_user[i].answer == "B") {
          this.answerColor[i][0] = "#fff";
          this.answerColor[i][1] = "#32ff7e";
          this.answerColor[i][2] = "#fff";
          this.answerColor[i][3] = "#fff";
        } else if (this.answers_list_user[i].answer == "C") {
          this.answerColor[i][0] = "#fff";
          this.answerColor[i][1] = "#fff";
          this.answerColor[i][2] = "#32ff7e";
          this.answerColor[i][3] = "#fff";
        } else if (this.answers_list_user[i].answer == "D") {
          this.answerColor[i][0] = "#fff";
          this.answerColor[i][1] = "#fff";
          this.answerColor[i][2] = "#fff";
          this.answerColor[i][3] = "#32ff7e";
        } else {
          this.answerColor[i][0] = "#fff";
          this.answerColor[i][1] = "#fff";
          this.answerColor[i][2] = "#fff";
          this.answerColor[i][3] = "#fff";
        }
      } else {
        if (this.answers_list_user[i].answer == "A") {
          this.answerColor[i][0] = "#ffcccc";
          this.answerColor[i][1] = "#fff";
          this.answerColor[i][2] = "#fff";
          this.answerColor[i][3] = "#fff";
        } else if (this.answers_list_user[i].answer == "B") {
          this.answerColor[i][0] = "#fff";
          this.answerColor[i][1] = "#ffcccc";
          this.answerColor[i][2] = "#fff";
          this.answerColor[i][3] = "#fff";
        } else if (this.answers_list_user[i].answer == "C") {
          this.answerColor[i][0] = "#fff";
          this.answerColor[i][1] = "#fff";
          this.answerColor[i][2] = "#ffcccc";
          this.answerColor[i][3] = "#fff";
        } else if (this.answers_list_user[i].answer == "D") {
          this.answerColor[i][0] = "#fff";
          this.answerColor[i][1] = "#fff";
          this.answerColor[i][2] = "#fff";
          this.answerColor[i][3] = "#ffcccc";
        } else {
          this.answerColor[i][0] = "#fff";
          this.answerColor[i][1] = "#fff";
          this.answerColor[i][2] = "#fff";
          this.answerColor[i][3] = "#fff";
        }
      }
    }
    console.log(this.answerColor);
  }

  onClickSubmit() {
    (<HTMLButtonElement>(
      document.getElementsByClassName("btn-submit")[0]
    )).disabled = true;
    this.sendCheckUserAnswerRequest();
  }
}
