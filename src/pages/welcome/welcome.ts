import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { GetStartedPage } from "../get-started/get-started";
import { Storage } from "@ionic/storage";
import { HomePage } from "../home/home";

@Component({
  selector: "page-welcome",
  templateUrl: "welcome.html"
})
export class WelcomePage {
  constructor(
    public navCtrl: NavController,
    private storage: Storage,
    public navParams: NavParams
  ) {
    setTimeout(() => {
      this.storage.get("user_id").then(value => {
        console.log("Welcome page get user_id: " + value);

        if (value != null) {
          //da dang nhap
          this.navCtrl.setRoot(HomePage);
        } else {
          //chua dang nhap
          this.navCtrl.setRoot(GetStartedPage);
        }
      });
    }, 2500);
  }
}
