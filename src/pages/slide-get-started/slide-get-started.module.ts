import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SlideGetStartedPage } from './slide-get-started';

@NgModule({
  declarations: [
    SlideGetStartedPage,
  ],
  imports: [
    IonicPageModule.forChild(SlideGetStartedPage),
  ],
})
export class SlideGetStartedPageModule {}
