import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";

import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { WelcomePage } from "../pages/welcome/welcome";
import { GetStartedPage } from "../pages/get-started/get-started";
import { SlideGetStartedPage } from "../pages/slide-get-started/slide-get-started";
import { LoginPage } from "../pages/login/login";
import { RegisterPage } from "../pages/register/register";
import { LessonListPage } from "../pages/lesson-list/lesson-list";
import { LessonPage } from "../pages/lesson/lesson";
import { KnowledgeTestPage } from "../pages/knowledge-test/knowledge-test";
import { HttpClientModule } from "@angular/common/http";
import { IonicStorageModule } from "@ionic/storage";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    WelcomePage,
    LoginPage,
    RegisterPage,
    SlideGetStartedPage,
    GetStartedPage,
    LessonPage,
    LessonListPage,
    KnowledgeTestPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    WelcomePage,
    SlideGetStartedPage,
    LoginPage,
    RegisterPage,
    GetStartedPage,
    LessonPage,
    LessonListPage,
    KnowledgeTestPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}
