# UIT-Streamline-App-Frontend

TDTL Ionic Mobile application

- Cài đặt Node.js + npm: https://nodejs.org/en/ 

- Cài đặt Ionic và Cordova: npm install -g ionic cordova

- Vào thư mục chứa project, chạy câu lệnh "npm install" trong CMD để cài đặt modules cần thiết

- Chạy "ionic serve" để xem ứng dụng trong Web browser

- Build ứng dụng sang điện thoại: ionic cordova run android --device --verbose

- Publish ứng dụng (tạo file .apk): https://ionicframework.com/docs/v1/guide/publishing.html
